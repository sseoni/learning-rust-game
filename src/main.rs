use std::io;
use rand::Rng;
use std::cmp;

fn main() {
    println!("Enter a number:");

    let secret_number = rand::thread_rng().gen_range(0..=10);

    loop {
        let mut input = String::new();

        io::stdin()
            .read_line(&mut input)
            .expect("bad input!");

        if input.trim() == "exit" {
            println!("goodbye!");
            break;
        }

        let input: u32 = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        match input.cmp(&secret_number) {
            cmp::Ordering::Greater => println!("Too high!"),
            cmp::Ordering::Less => println!("Too low!"),
            cmp::Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
